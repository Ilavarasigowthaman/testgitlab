package com.excell;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.reporters.jq.Main;

import io.github.bonigarcia.wdm.WebDriverManager;


	public class Loginn {
	 public static void main(String[] args) throws IOException {
	        WebDriverManager.firefoxdriver().setup();
	        WebDriver driver = new FirefoxDriver();
	        driver.get("https://property-management-admin.web.app/");

	        String fileLocation = "./TestData//Login.xlsx";
	        String sheetName = "sheet1"; // Replace with your sheet name
	        Object[][] excelData = ReadExcel.readExcelData(fileLocation, sheetName);

	        for (Object[] rowData : excelData) {
	            String username = (String) rowData[0]; // Assuming username is in the first column
	            String password = (String) rowData[1]; // Assuming password is in the second column

	            // Your login logic here using username and password
	            driver.findElement(By.name("emailAddress")).sendKeys(username);
	            driver.findElement(By.name("password")).sendKeys(password);
	            driver.findElement(By.id("kt_sign_in_submit")).click();

	            // Add any additional logic you need after login
	        }

	        driver.quit();
	    }
	}
