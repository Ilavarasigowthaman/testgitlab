package com.excell;

import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

 
	
	/*String fileLocation="./TestData//Login.xlsx";
	
	XSSFWorkbook wbook=new XSSFWorkbook(fileLocation);
	XSSFSheet sheet=wbook.getSheetAt(0);
	int lastRowNumber=sheet.getLastRowNum();
	System.out.println(lastRowNumber);
short lastCell=	sheet.getRow(0).getLastCellNum();
System.out.println(lastCell);
Object[][] data = new Object[lastRowNumber][lastCell];

	for (int i = 1; i <= lastRowNumber; i++) {
		XSSFRow row = sheet.getRow(i);
		
		for (int j = 0; j < lastCell; j++) {
			XSSFCell cell = row.getCell(j);
			DataFormatter dft=new DataFormatter();
			String value =dft.formatCellValue(cell);
			System.out.println(value);
		} 
	}
	
wbook.close();
return data;
}}
*/
public class ReadExcel {
    	public static Object[][] readExcelData(String fileLocation, String sheetName) throws IOException {
            XSSFWorkbook wbook = new XSSFWorkbook(fileLocation);
            XSSFSheet sheet = wbook.getSheet(sheetName);

            int lastRowNumber = sheet.getLastRowNum();
            short lastCell = sheet.getRow(0).getLastCellNum();

            Object[][] data = new Object[lastRowNumber][lastCell];

            for (int i = 1; i <= lastRowNumber; i++) {
                XSSFRow row = sheet.getRow(i);

                for (int j = 0; j < lastCell; j++) {
                    XSSFCell cell = row.getCell(j);
                    DataFormatter dft = new DataFormatter();
                    String value = dft.formatCellValue(cell);
                    data[i - 1][j] = value;
                }
            }

            wbook.close();
            return data;
        }
    
}






